<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use udeclass\Services\NotesServices;


class Notes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'udeclass:remembers-notes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A las 10 am revisa todos las notas que se deben enviar ese dia y las envia';

    /** @var  ActivitiesService */
    private $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(NotesServices $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->service->sendNotifications();

    }
}