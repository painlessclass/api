<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use udeclass\Services\HomeworkServices;


class Homeworks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'udeclass:remembers-homeworks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A las 10 am revisa todos las tareas que se deben enviar ese dia y las envia';

    /** @var  ActivitiesService */
    private $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(HomeworkServices $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->service->sendHomeWorksNotifications();

    }
}