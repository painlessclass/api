<?php

namespace udeclass\Models;

use Illuminate\Database\Eloquent\Model;
use udeclass\Helpers\CurrentUser;

class Matter extends Model
{
    var $table = "matter";

    public function schedule()
    {
        return $this->hasMany('udeclass\Models\Schedule', 'id_matter', 'id') ;
    }

    public function homework()
    {
        return $this->hasMany('udeclass\Models\Homework', 'id_matter', 'id');
    }

    public function Test()
    {
        return $this->hasMany('udeclass\Models\Test', 'id_matter', 'id');
    }

    public function Notes()
    {
        return $this->hasMany('udeclass\Models\Notes', 'id_matter', 'id');
    }
    public function Forums()
    {
        return $this->hasMany('udeclass\Models\Forum', 'matter_id', 'id');
    }
}

