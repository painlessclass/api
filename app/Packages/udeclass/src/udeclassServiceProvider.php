<?php

namespace udeclass;

use Illuminate\Support\ServiceProvider;

class udeclassServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadRoutesFrom(__DIR__ . "/Routes/api.php");

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {


    }
}
