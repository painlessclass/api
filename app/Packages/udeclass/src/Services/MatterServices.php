<?php

namespace udeclass\Services;


use function foo\func;
use Illuminate\Support\Facades\DB;
use LaravelFCM\Facades\FCMGroup;
use udeclass\Helpers\CurrentUser;
use udeclass\Http\Controllers\SubscriptionUserToMatterController;
use udeclass\Models\Matter;
use udeclass\Models\Schedule;

class MatterServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Matter());
    }
    public function createMatter($arrData)
    {
        $model = new Matter();
        $user = CurrentUser::getFromJWT();

        foreach ($arrData as $column => $value) {

            if($column == "schedule")
            {
                continue;
            }
            if($column == "idCareer")
            {
                $model->id_career = $value;
                continue;
            }
            $model->$column = $value;
        }

        $model->create_by = $user->id;

        $model->save();

        $objUser = CurrentUser::getFromJWT();
        $tokens = [$objUser->fcm_token];

        $groupName = $model->id . '_' .$arrData['name'];


        // Save notification key in your database you must use it to send messages or for managing this group
        $notification_key = FCMGroup::createGroup($groupName, $tokens);

        $model->group_key = $notification_key;

        $model->save();

        foreach ($arrData as $column => $value)
        {
            if($column == "schedule")
            {
                foreach ($value as $days)
                {
                    $modelSchedule = new Schedule();

                    $modelSchedule->dia = $days['dia'];
                    $modelSchedule->hora_inicio = $days['hora_inicio'];
                    $modelSchedule->hora_fin = $days['hora_fin'];
                    $modelSchedule->id_matter = $model->id;

                    $modelSchedule->save();

                }
            }
        }

        return $this->subscriptionUserToMatter($model->id);
    }

    public function getAllMatterByClientId()
    {
        $objUser = CurrentUser::getFromJWT();

        return $objUser->matters()->where('matter_user.deleted','<>',1)->get();

    }

    public function getAllMatterPublic($data)
    {
        $matterFromUser = $this->getAllMatterByClientId();

        $arrIds = array();
        foreach ($matterFromUser as $matter)
        {
            array_push($arrIds, $matter['attributes']['id']);
        }

        $response = DB::table('matter')
        ->select('*')
        ->whereNotIn('id',$arrIds)
        ->where('private', '<>', 1)
        ->where('deleted', '<>', 1)
        ->where('id_career', '=',$data['idCareer'] )
        ->get();




        return $response;

        /*return DB::table('matter')
                ->select('*')
                ->leftJoin('matter_user', function($join){
                    $join->on('matter_user.matter_id', '=', 'matter.id');
                })
                ->whereNull('matter.id')
                ->get();*/
        /*return DB::table('matter')
            ->join('inscription_users_matter', 'matter.id', 'inscription_users_matter.id_mater')
            ->select('matter.*')
            ->where('matter.private', '=',0)
            ->where('matter.deleted', '<>',1)
            ->where('inscription_users_matter.id_user', '<>',$user->id)
            ->get();*/

    }

    public function subscriptionUserToMatter($idMatter)
    {
        $model = Matter::find($idMatter);

        $user = CurrentUser::getFromJWT();

        $tokens = [$user->fcm_token];
        $groupName = $model->id . '_' .$model->name;
        $notificationKey = $model->group_key;

        $key = FCMGroup::addToGroup($groupName, $notificationKey, $tokens);
        return $user->matters()->save($model);
    }

    /*public function getNotas(matter_id){
        $objUser = CurrentUser::getFromJWT();
        $objMatter = Matter::find(matter_id);
        $objMatter->notas(['create_by', '=', $objUser->id],['is_public', '=', true]);

        return $objMatter;

    }*/

/*    public function deleteCareerById($data)
    {
        $model = Career::find($data['id_career']);
        $model->deleted = 1;

        return $model->save();

    }*/

}