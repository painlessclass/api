<?php

namespace udeclass\Services;

use Illuminate\Support\Facades\Schema;
use udeclas\Exceptions\BaseException;
use udeclass\Helpers\CurrentUser;


class GenericService
{

    var $objModel;
    var $strClassName;

    function __construct($objModel)
    {

        $this->objModel = $objModel;
        $this->strClassName = get_class($objModel);
    }

    public function store($arrData, $bolReturnSaved = false)
    {

        $objModel = $this->captureData($arrData);

        $objUser = $this->getUser();

        $objModel->created_by = $objUser->id;
        $objModel->modified_user_id = $objUser->id;

        $objModel->save();

        if ($bolReturnSaved === TRUE) {
            $objModel = $objModel->find($objModel->id);
        }

        return $objModel;

    }

    public function captureData($arrData)
    {

        $objModel = new $this->strClassName;

        $arrFields = array_flip(Schema::getColumnListing($objModel->getTable()));

        $arrDataToModel = array_intersect_key($arrData, $arrFields);

        if (is_array($arrDataToModel)) {
            foreach ($arrDataToModel as $strField => $strValue) {

                if ($strField == 'id') {
                    continue;
                }

                $objModel->setAttribute($strField, $strValue);

            }
        }


        return $objModel;

    }

    public function getUser()
    {

        $objUser = CurrentUser::getFromJWT();

        return $objUser;

    }

    public function getRecordById($idRecord)
    {

        $objModel = new $this->strClassName;

        $objRecord = $objModel::find($idRecord);

        if ($objRecord === NULL) {
            throw new BaseException("NOT_FOUND", "Record not found");
        }

        return $objRecord;

    }

    public function deleteRecordById($idRecord)
    {

        $objModel = new $this->strClassName;

        return $objModel->destroy($idRecord);

    }

    public function getAll($arrOrder = [])
    {
        $objModel = new $this->strClassName;

        $objQuery = $objModel->newQuery();

        if (!empty($arrOrder['order_by'])) {
            if (empty($arrOrder['order'])) {
                $objQuery = $objQuery->orderBy($arrOrder['order_by']);
            } else {
                $objQuery = $objQuery->orderBy($arrOrder['order_by'], $arrOrder['order']);
            }

        }

        return $objQuery->get();

    }

    public function update($id, $arrData)
    {

        $objModel = new $this->strClassName;

        $objRecord = $objModel::find($id);

        foreach ($arrData as $strField => $strValue) {
            $objRecord->setAttribute($strField, $strValue);
        }

        return $objRecord->save();

    }

    public function search($arrFields = [], $arrFilters = [], $arrPagination = [], $arrOrder = [])
    {

        if (empty($arrFields)) {
            $arrFields = ['*'];
        }

        $intPage = 1;
        $intPerPage = 15;

        if (!empty($arrPagination['itemsPerPage']) && ctype_alnum($arrPagination['itemsPerPage'])) {
            $intPerPage = intval($arrPagination['itemsPerPage']);
        }

        if (!empty($arrPagination['currentPage']) && ctype_alnum($arrPagination['currentPage'])) {
            $intPage = intval($arrPagination['currentPage']);
        }

        $objModel = new $this->strClassName;

        $objQuery = $objModel->newQuery();

        if (!empty($arrFilters) && is_array($arrFilters)) {
            foreach ($arrFilters as $arrFilter) {
                list($strField, $strOperator, $strValue) = $arrFilter;
                if (!empty($strValue)) {
                    $objQuery = $objQuery->where($strField, $strOperator, $strValue);
                }
            }
        }

        if (!empty($arrOrder['order_by'])) {
            if (empty($arrOrder['order'])) {
                $objQuery = $objQuery->orderBy($arrOrder['order_by']);
            } else {
                $objQuery = $objQuery->orderBy($arrOrder['order_by'], $arrOrder['order']);
            }

        }

        $objPaginator = $objQuery->paginate($intPerPage, $arrFields, 'page', $intPage);

        return $objPaginator->toArray();

    }

}