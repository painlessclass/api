<?php

namespace udeclass\Services;

use Carbon\Carbon;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;
use udeclass\Helpers\CurrentUser;
use udeclass\Models\Homework;
use udeclass\Models\Matter;
use udeclass\Models\Test;

class TestServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Test());
    }

    public function createTest($data)
    {
        $user = CurrentUser::getFromJWT();
        $model = new Test();

        foreach ($data as $column => $value)
        {
            if($column == "alarmStart")
            {
                $model->alarm_start = $value;
                continue;
            }
            if($column == "beforeTime")
            {
                $model->before_time= $value;
                continue;
            }
            if($column == "idMatter")
            {
                $model->id_matter= $value;
                continue;
            }

            $model->$column = $value;
        }

        $model->id_user_created = $user->id;

        $matter =  Matter::find($data['idMatter']);

        $notificationKey = $matter->group_key;

        $notificationBuilder = new PayloadNotificationBuilder($matter->name . ' - Tienes una nueva Evaluación');
        $notificationBuilder->setBody($data['description'] .' Para la fecha ' . $data['alarmStart'])
            ->setSound('default');

        $notification = $notificationBuilder->build();


        FCM::sendToGroup($notificationKey, null, $notification, null);


        return $model->save();


    }

    public function getAllTest($params)
    {
        $user = CurrentUser::getFromJWT();

        return Matter::find($params['id_matter'])
            ->Test()
            ->where('test.cumplida', '<>', 1)
            ->orderBy('created_at', 'desc')
            //->where('test.id_user_created', '=', $user->id)
            ->get();
    }

    public function updateTestById($params)
    {
        $testModel = Test::find($params[0]);
        $testModel->cumplida = 1;

        return $testModel->save();
    }

    function sendNotifications(){
        $tests = Test::where('notified',' =', 0)->get();

        $toNotify = [];
        foreach ($tests as $test){
            if($test->before_time == 'DIA_INDICADO'){

                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($test->alarm_start)->toDateString()){
                    array_push($toNotify,$test);
                }
            }
            if($test->before_time == '1D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($test->alarm_start)->subDay(1)->toDateString()){
                    array_push($toNotify,$test);
                }
            }
            if($test->before_time == '2D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($test->alarm_start)->subDay(2)->toDateString()){
                    array_push($toNotify,$test);
                }
            }
            if($test->before_time == '3D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($test->alarm_start)->subDay(3)->toDateString()){
                    array_push($toNotify,$test);
                }
            }

        }
        foreach ($toNotify as $notification){
            $matter =  Matter::find($notification['id_matter']);

            $notificationKey = $matter->group_key;


            $notificationBuilder = new PayloadNotificationBuilder($matter->name . ' - Recordatorio Evaluación');
            $notificationBuilder->setBody($notification['description'])
                ->setSound('default');

            $notificationFCM = $notificationBuilder->build();

            FCM::sendToGroup($notificationKey, null, $notificationFCM, null);

            $objNotification = Test::find($notification->id);
            $objNotification->notified = 1;
            $objNotification->save();
        }

    }


}