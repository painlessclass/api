<?php

namespace udeclass\Services;

use Carbon\Carbon;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;
use udeclass\Helpers\CurrentUser;
use udeclass\Models\Matter;
use udeclass\Models\Notes;


class NotesServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Notes());
    }

    public function createNotes($data)
    {
        $user = CurrentUser::getFromJWT();
        $model = new Notes();

        foreach ($data as $column => $value)
        {
            if($column == "idMatter")
            {
                $model->id_matter = $value;
                continue;
            }
            if($column == "alarmStart")
            {
                $model->alarm_start = $value;
                continue;
            }
            if($column == "beforeTime")
            {
                $model->before_time = $value;
                continue;
            }
            if($column =="img")
            {
                $model->$column = $value;
                continue;
            }
            $model->$column = $value;
        }

        $model->id_user_created = $user->id;


        $matter =  Matter::find($data['idMatter']);

        $notificationKey = $matter->group_key;

        $notificationBuilder = new PayloadNotificationBuilder($matter->name . '- Tienes una nueva Nota');
        $notificationBuilder->setBody($data['name'])
            ->setSound('default');

        $notification = $notificationBuilder->build();


        FCM::sendToGroup($notificationKey, null, $notification, null);

        return $model->save();


    }

    public function getAllNotes($params)
    {
        $user = CurrentUser::getFromJWT();

        return Matter::find($params['id_matter'])
            ->Notes()
            ->where('notes.cumplida', '<>', 1)
            ->orderBy('created_at', 'desc')
            //->where('notes.id_user_created', '=', $user->id)
            ->get();
    }

    public function updateNotesById($params)
    {
        $NotesModel = Notes::find($params[0]);
        $NotesModel->cumplida = 1;

        return $NotesModel->save();
    }
    function sendNotifications(){
        $tests = Notes::where('notified',' =', 0)->get();

        $toNotify = [];
        foreach ($tests as $note){
            if($note->before_time == 'DIA_INDICADO'){

                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($note->alarm_start)->toDateString()){
                    array_push($toNotify,$note);
                }
            }
            if($note->before_time == '1D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($note->alarm_start)->subDay(1)->toDateString()){
                    array_push($toNotify,$note);
                }
            }
            if($note->before_time == '2D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($note->alarm_start)->subDay(2)->toDateString()){
                    array_push($toNotify,$note);
                }
            }
            if($note->before_time == '3D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($note->alarm_start)->subDay(3)->toDateString()){
                    array_push($toNotify,$note);
                }
            }

        }
        foreach ($toNotify as $notification){
            $matter =  Matter::find($notification['id_matter']);

            $notificationKey = $matter->group_key;


            $notificationBuilder = new PayloadNotificationBuilder($matter->name . ' - Recordatorio de Nota');
            $notificationBuilder->setBody($notification['description'])
                ->setSound('default');

            $notificationFCM = $notificationBuilder->build();

            FCM::sendToGroup($notificationKey, null, $notificationFCM, null);

            $objNotification = Notes::find($notification->id);
            $objNotification->notified = 1;
            $objNotification->save();
        }

    }

}