<?php

namespace udeclass\Services;


use Carbon\Carbon;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;
use udeclass\Helpers\CurrentUser;
use udeclass\Models\Homework;
use udeclass\Models\Matter;

class HomeworkServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Homework());
    }


    public function getAllHomeworkByIdMatter($params)
    {
        $user = CurrentUser::getFromJWT();

        return Matter::find($params['id_matter'])
            ->homework()
            ->where('homework.realizada', '<>', $params['realizada'])
            ->where('homework.id_user_created', '=', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function updateHomework($params)
    {
        $modelHomework = Homework::find($params[0]);
        $modelHomework->realizada = 1;

        return $modelHomework->save();
    }

    public function createHomework($data)
    {
        $user = CurrentUser::getFromJWT();
        $model = new Homework();

        foreach ($data as $column => $value)
        {
            if($column == "idMatter")
            {
                $model->id_matter = $value;
                continue;
            }
            if($column == "alarmStart")
            {
                $model->alarm_start = $value;
                continue;
            }
            if($column == "beforeTime")
            {
                $model->before_time = $value;
                continue;
            }
            $model->$column = $value;
        }

        $model->id_user_created = $user->id;


        return $model->save();


    }

    function sendHomeWorksNotifications(){
        $homeworks = Homework::where('notified',' =', 0)->get();

        $toNotify = [];
        foreach ($homeworks as $homework){
            if($homework->before_time == 'DIA_INDICADO'){

                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($homework->alarm_start)->toDateString()){
                    array_push($toNotify,$homework);
                }
            }
            if($homework->before_time == '1D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($homework->alarm_start)->subDay(1)->toDateString()){
                    array_push($toNotify,$homework);
                }
            }
            if($homework->before_time == '2D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($homework->alarm_start)->subDay(2)->toDateString()){
                    array_push($toNotify,$homework);
                }
            }
            if($homework->before_time == '3D'){
                if (Carbon::now()->timezone('America/Bogota')->toDateString() == Carbon::parse($homework->alarm_start)->subDay(3)->toDateString()){
                    array_push($toNotify,$homework);
                }
            }

        }
        foreach ($toNotify as $notification){
            $matter =  Matter::find($notification['id_matter']);

            $notificationKey = $matter->group_key;


            $notificationBuilder = new PayloadNotificationBuilder($matter->name . ' - Recordatorio Tarea');
            $notificationBuilder->setBody($notification['description'])
                ->setSound('default');

            $notificationFCM = $notificationBuilder->build();

            FCM::sendToGroup($notificationKey, null, $notificationFCM, null);

            $objNotification = Homework::find($notification->id);
            $objNotification->notified = 1;
            $objNotification->save();
        }

    }

}