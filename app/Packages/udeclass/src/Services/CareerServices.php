<?php

namespace udeclass\Services;


use Illuminate\Support\Facades\DB;
use udeclass\Models\Career;

class CareerServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Career());
    }
    public function createCareer($arrData)
    {
        $model = new Career();
        $model->name = $arrData['career'];
        $model->save();

        return $model;
    }

    public function getAllCareers()
    {
        return DB::table('career')
            ->select('*')
            ->where('deleted', '<>', 1)
            ->get();
    }

    public function deleteCareerById($data)
    {
        $model = Career::find($data['id_career']);
        $model->deleted = 1;

        return $model->save();

    }

}