<?php

namespace udeclass\Services;


use App\User;
use Illuminate\Support\Facades\DB;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;
use udeclass\Helpers\CurrentUser;
use udeclass\Models\Forum   ;
use udeclass\Models\Matter;

class ForumService extends GenericService
{
    function __construct()
    {
        parent::__construct(new Forum());
    }

    public function store($arrData, $bolReturnSaved = false)
    {
        $objModel =  new Forum();

        $objUser = CurrentUser::getFromJWT();

        $objModel->user_id = $objUser->id;
        $objModel->matter_id = $arrData['idMatter'];
        $objModel->message = $arrData['description'];

        $objModel->save();

        $matter =  Matter::find($arrData['idMatter']);

        $notificationKey = $matter->group_key;

        $notificationBuilder = new PayloadNotificationBuilder($matter->name . '- Tienes un nuevo Mensaje');
        $notificationBuilder->setBody($arrData['description'])
            ->setSound('default');

        $notification = $notificationBuilder->build();


        FCM::sendToGroup($notificationKey, null, $notification, null);

        return $objModel;

    }
    public function getFromMatter($arrData){
        $matter =  new Matter();
        $id_matter = $arrData['id_matter'];
        $objMatter = $matter->find($id_matter);

        $forums = $objMatter->forums()->orderBy('created_at', 'desc')->take(10)->get();
        foreach ($forums as  $key=>$value){
            $forums[$key]['user'] = User::find($value->user_id);
        }
        return $forums;
    }


}