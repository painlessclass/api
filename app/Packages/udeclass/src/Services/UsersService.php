<?php
/**
 * TiTo
 * 3/8/2017 6:29 PM
 */

namespace udeclass\Services;


use App\User;
use Illuminate\Support\Facades\DB;
use udeclass\Models\Permission;

use udeclass\Models\Group;


class UsersService extends GenericService
{

    function __construct()
    {
        parent::__construct(new User());
    }

    public function create(array $arrData)
    {
        $objUser = new User();

        $objUser->name = $arrData['name'];
        $objUser->password = bcrypt($arrData['password']);
        $objUser->email = $arrData['email'];
        $objUser->fcm_token = $arrData['fcm_token'];
        $objUser->save();
        return $objUser;

    }

    public function updateUser($id, $arrParams)
    {

        $objModel = $this->getRecordById($id, $arrParams);

        foreach ($arrParams as $strField => $strValue) {
            if ($strField == 'password') {
                $strValue = bcrypt($strValue);
            }
            $objModel->setAttribute($strField, $strValue);
        }

        $objModel->save();

        return $objModel;

    }

    public function addRolUser($idUserRelation, $arrData)
    {
        $modelUserRelationRole = new UserRelationRole();

        $modelUserRelationRole->role_id = $arrData['role_id'];
        $modelUserRelationRole->user_id = $idUserRelation;

        return $modelUserRelationRole->save();
    }

    public function getGroups($idUser)
    {

        $objUser = User::find($idUser);

        return $objUser->groups;


    }

    public function getAllRolesByUserId($idUser)
    {
        return DB::table('acl_roles_users')
            ->select('acl_roles.name')
            ->join('acl_roles', 'acl_roles_users.role_id', 'acl_roles.id')
            ->where('user_id', '=', $idUser)
            ->get();
    }

    public function addGroup($idUser, $idGroup)
    {

        $objUser = User::find($idUser);

        $objGroup = Group::find($idGroup);

        $objUser->groups()->save($objGroup);

    }

    public function getACL(User $objUser)
    {

        $colPermissions = Permission::all();

//        sd($colPermissions);

        $arrACL = [];

        foreach ($colPermissions as $objPermission) {
            $arrACL[$objPermission->name] = $objUser->can($objPermission->name);
        }

        return $arrACL;

    }

    public function getAllRoles()
    {
        return DB::table('acl_roles')
            ->select('*')
            ->where('deleted', '<>', 1)
            ->orderBy('name')
            ->get();
    }



}