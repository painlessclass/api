<?php

namespace udeclass\Services;


use Illuminate\Support\Facades\DB;
use udeclass\Helpers\CurrentUser;
use udeclass\Models\Career;
use udeclass\Models\Matter;
use udeclass\Models\Schedule;

class ScheduleServices extends GenericService
{
    function __construct()
    {
        parent::__construct(new Schedule());
    }

    public function getAllMatterSchedule()
    {
        $matterService = new MatterServices();
        $matters = $matterService->getAllMatterByClientId();

        $arrMatter = [];
        foreach ($matters as $matter) {
            $result = Matter::find($matter['attributes']['id'])->schedule()->orderBy('dia','asc')->orderBy('hora_inicio','asc')->get();

            $arrMatter['matter'] = $matter['attributes'];

            $arrSchedule = [];
            foreach ($result as $item) {
                $arrSchedule[] = $item['attributes'];
            }

            $arrMatter['matter']['schedule'] = $arrSchedule;

            $response[] = $arrMatter;

        }
        
        return $response;
    }
/*
    public function getAllCareers()
    {
        return DB::table('career')
            ->select('*')
            ->where('deleted', '<>', 1)
            ->get();
    }

    public function deleteCareerById($data)
    {
        $model = Career::find($data['id_career']);
        $model->deleted = 1;

        return $model->save();

    }*/

}