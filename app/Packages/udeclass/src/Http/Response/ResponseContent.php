<?php

namespace udeclass\Http\Response;

class ResponseContent
{

    var $status = null;
    var $error = null;
    var $data = null;

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setError(ErrorDetails $error)
    {
        $this->error = $error;
    }

    public function __toString()
    {

        if ($this->status == null) {
            unset($this->status);
        }

        if ($this->error == null) {
            unset($this->error);
        }

        if ($this->data == null) {
            unset($this->data);
        }

        return json_encode($this);
    }

}