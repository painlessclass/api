<?php

namespace udeclass\Http\Response;

use Illuminate\Http\Response;
use udeclas\Exceptions\BaseException;
use udeclass\Http\Response\ResponseContent;

class ResponseBuilder
{

    var $objContent;
    var $httpStatusCode;

    function __construct()
    {
        $this->objContent = new ResponseContent();

    }

    public function setStatusSuccess()
    {
        $this->httpStatusCode = 200;
        $this->objContent->setStatus("success");
    }

    public function setStatusCreated($idRecord)
    {
        $this->httpStatusCode = 201;
        $this->setStatusSuccess();
        $this->objContent->setData(['id' => $idRecord]);
    }

    public function setData($varData)
    {
        $this->objContent->setData($varData);
    }

    public function setStatusError($strErrorCode, $strErrorDescription)
    {
        $this->setHttpStatusCode(422);
        $this->objContent->setStatus("error");
        $this->objContent->setError(new ErrorDetails($strErrorCode, $strErrorDescription));
    }

    public function setHttpStatusCode($intCode)
    {
        $this->httpStatusCode = $intCode;
    }

    public function setFromBaseException(BaseException $exception)
    {
        $this->httpStatusCode = 422;
        $this->setStatusError($exception->getKey(), $exception->getDescription());
    }

    public function setFromFatalException(\Exception $exception)
    {
        $this->httpStatusCode = 500;
        $this->setStatusError($exception->getCode(), $exception->getMessage());
    }

    public function buildResponse()
    {

        $objHttpResponse = new Response();
        $objHttpResponse->withHeaders(['Content-Type' => 'application/json']);
        $objHttpResponse->setStatusCode($this->httpStatusCode);
        $objHttpResponse->setContent($this->objContent);

        return $objHttpResponse;

    }

}