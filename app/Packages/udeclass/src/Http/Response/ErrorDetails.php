<?php

namespace udeclass\Http\Response;

class ErrorDetails
{

    var $code = null;
    var $message = null;

    public function __construct($strCode, $strMessage)
    {
        $this->code = $strCode;
        $this->message = $strMessage;
    }

    public function __toString()
    {
        return json_encode($this);
    }

}