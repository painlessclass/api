<?php

namespace udeclass\Http\Controllers;

use Illuminate\Http\Request;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\TestServices;

class TestController extends GenericRestController
{
    function __construct()
    {
        $this->setService(new TestServices());
    }

    public function createTest(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $arrData = $request->input();

        try {
            $response = $this->objService->createTest($arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }



        return $objResponseBuilder->buildResponse();
    }

    public function getAllTest(Request $request, ResponseBuilder $objResponseBuilder)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->getAllTest($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function updateTestById(Request $request, ResponseBuilder $objResponseBuilder)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->updateTestById($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }
}
