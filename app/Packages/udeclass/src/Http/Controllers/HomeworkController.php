<?php

namespace udeclass\Http\Controllers;

use Illuminate\Http\Request;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\HomeworkServices;

class HomeworkController extends GenericRestController
{
    function __construct()
    {
        $this->setService(new HomeworkServices());
    }

    public function getAllHomeworkByIdMatter(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->getAllHomeworkByIdMatter($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function updateHomework(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->updateHomework($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function createHomework(Request $request, ResponseBuilder $objResponseBuilder)
    {
        $arrData = $request->input();

        try {
            $response = $this->objService->createHomework($arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }



        return $objResponseBuilder->buildResponse();
    }
}
