<?php
/**
 * TiTo
 * 3/8/2017 6:29 PM
 */

namespace p7days2go\Http\Controllers;


use Illuminate\Http\Request;
use p7days2go\Http\Response\ResponseBuilder;
use p7days2go\Services\RoleService;

class RoleController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new RoleService());
    }

    function fullList(Request $request){
        $arrOrdering = [];
        $arrOrdering['order_by'] = $request->input('order_by');
        $arrOrdering['order'] = $request->input('order');

        $arrResponse = [];

        try {
            $arrData = $this->objService->getAll($arrOrdering);
        } catch (\Exception $e) {
            $arrResponse['status'] = 'error';
            $arrResponse['message'] = $e->getMessage();
            return $arrResponse;
        }

        $arrResponse['status'] = 'success';
        $arrResponse['data'] = $arrData;

        return $arrResponse;
    }


}