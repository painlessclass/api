<?php

namespace udeclass\Http\Controllers;

use App\Forum;
use Illuminate\Http\Request;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\ForumService;

class ForumController extends GenericRestController
{
    function __construct()
    {
        $this->setService(new ForumService());
    }

    public function getAll(Request $request)
    {
        $objResponseBuilder = new ResponseBuilder();
        $arrParams = $request->input();

        try {
            $colRecords = $this->objService->getAll($arrParams);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($colRecords);
        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function getFromMatter(Request $request)
    {
        $objResponseBuilder = new ResponseBuilder();
        $arrParams = $request->input();

        try {
            $colRecords = $this->objService->getFromMatter($arrParams);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($colRecords);
        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }
}
