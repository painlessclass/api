<?php
/**
 * Ezesubu
 * 3/8/2017 6:29 PM
 */

namespace udeclass\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use udeclass\Http\Controllers\GenericRestController;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\MatterServices;

class MatterController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new MatterServices());
    }

    public function createMatter(ResponseBuilder $objResponseBuilder,Request $request)
    {

        $arrData = $request->input();

        try {
            $response = $this->objService->createMatter($arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }



        return $objResponseBuilder->buildResponse();

    }

    public function subscriptionUserToMatter(ResponseBuilder $objResponseBuilder,Request $request)
    {

        $arrData = $request->input();

        try {
            $response = $this->objService->subscriptionUserToMatter($arrData['idMatter']);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }



        return $objResponseBuilder->buildResponse();

    }

    public function getAllMatterByClientId(ResponseBuilder $objResponseBuilder)
    {
        try {

            $objMatter = $this->objService->getAllMatterByClientId();

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function getAllMatterPublic(ResponseBuilder $objResponseBuilder,Request $request)
    {
        $data = ((array) json_decode($request->input('data')));

        try {

            $objMatter = $this->objService->getAllMatterPublic($data);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function deleteCareerById(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $data = ((array)json_decode($request->input('data'))) ;
        try {
            $reponse = DB::table('career')
                ->where('id', '=', $data['id_career'])
                ->update(['deleted' => 1]);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($reponse);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

}