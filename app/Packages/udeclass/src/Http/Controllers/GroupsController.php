<?php
/**
 * TiTo
 * 3/8/2017 6:29 PM
 */

namespace p7days2go\Http\Controllers;


use Illuminate\Http\Request;
use p7days2go\Http\Response\ResponseBuilder;
use p7days2go\Services\GroupsService;

class GroupsController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new GroupsService());
    }

    public function create(Request $request)
    {

        $arrData = $request->input();

        $objService = new GroupsService();

        $objUser = $objService->create($arrData);

        $objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusCreated($objUser->id);

        return $objResponseBuilder->buildResponse();

    }

    public function getGroupsByType(Request $request)
    {
        $objResponseBuilder = new ResponseBuilder();
		$arrParams = $request->input();
	
        try {
            $colRecords = $this->objService->getGroupsByType($arrParams);

			$objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($colRecords);
        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }


}