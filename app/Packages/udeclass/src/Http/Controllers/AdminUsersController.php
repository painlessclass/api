<?php
/**
 * Ezesubu
 * 3/8/2017 6:29 PM
 */

namespace p7days2go\Http\Controllers;


use Illuminate\Http\Request;
use p7days2go\Http\Response\ResponseBuilder;
use p7days2go\Services\UsersService;

class AdminUsersController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new UsersService());
    }

    public function create(Request $request)
    {

        $arrData = $request->input();

        $objService = new UsersService();

        $objUser = $objService->create($arrData);

        $objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusSuccess();
        $objResponseBuilder->setData($objUser);

        return $objResponseBuilder->buildResponse();

    }

    public function updateUser(Request $request, ResponseBuilder $objResponseBuilder, $id)
    {
        $arrData = $request->input();

        try {

            $objRoles = $this->objService->updateUser($id, $arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($objRoles);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();




    }

    public function addRolUser(Request $request, ResponseBuilder $objResponseBuilder, $idUser)
    {
        $arrData = $request->input();

        try {

            $objRoles = $this->objService->addRolUser($idUser, $arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($objRoles);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function deleteAssign(ResponseBuilder $objResponseBuilder,$idRelation)
    {
        try {

            $objRelation = $this->objService->deleteAssignCountry($idRelation);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objRelation);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function deleteAssignGroup(ResponseBuilder $objResponseBuilder,$idRelation)
    {
        try {

            $objRelation = $this->objService->deleteAssignGroup($idRelation);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objRelation);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function getUserCountries($idUser)
    {

        $objService = new UsersService();

        $colCountries = $objService->getCountries($idUser);

        $objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusSuccess();
        $objResponseBuilder->setData($colCountries);

        return $objResponseBuilder->buildResponse();

    }

    public function getAllRoles(ResponseBuilder $objResponseBuilder)
    {

        try {

            $objRoles = $this->objService->getAllRoles();

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objRoles);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function addUserCountries(Request $request, $idUser)
    {

        $arrData = $request->input();

        $objService = new UsersService();

        $objService->addContries($idUser, $arrData);

        $objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusSuccess();

        return $objResponseBuilder->buildResponse();

    }

    public function getUserGroups(ResponseBuilder $objResponseBuilder,$idUser)
    {

        try {

            $colCountries = $this->objService->getGroups($idUser);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($colCountries);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();


    }

    public function addUserGroups(Request $request, $idUser)
    {

        $idGroup = $request->input('idGroup');

        $objService = new UsersService();

        $objService->addGroup($idUser, $idGroup);

        $objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusSuccess();

        return $objResponseBuilder->buildResponse();

    }

    public function getAllRolesByUserId(ResponseBuilder $objResponseBuilder,$idUser)
    {

        try {

            $objRoles = $this->objService->getAllRolesByUserId($idUser);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objRoles);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }


}