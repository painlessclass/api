<?php

namespace udeclass\Http\Controllers;

use udeclas\Exceptions\BaseException;
use udeclass\Http\Response\ResponseBuilder;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use udeclass\Service\GenericService;


class GenericRestController extends Controller
{

    /** @var  GenericService $objService */
    var $objService;

    public function getCurrentUser(){

        return JWTAuth::parseToken()->authenticate();

    }

    public function setService($objService)
    {
        $this->objService = $objService;
    }

    public function store(Request $request)
    {

        $objResponse = new Response();

        $arrInput = $request->input();

        try {
            $objModel = $this->objService->store($arrInput);
        } catch (\Exception $e) {

            $objResponse->setStatusCode(422);

            $objResponse->setContent([
                'status' => 'error',
                'error' => [
                    'type' => 'INVALID_INPUT',
                    'message' => $e->getMessage()
                ]
            ]);

            return $objResponse;

        }

        $objResponse->setStatusCode(201);

        $objResponse->setContent([
            'status' => 'success',
            'data' => ['id' => $objModel->getAttribute('id')]
        ]);

        return $objResponse;

    }

    public function show(Request $request, $id)
    {

        $arrParams = $request->input();

        $objResponseBuilder = new ResponseBuilder();

        try {

            $objModel = $this->objService->getRecordById($id, $arrParams);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objModel);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();


    }

    public function getOne(Request $request, $id)
    {

        $arrParams = $request->input();

        $objResponseBuilder = new ResponseBuilder();

        try {

            $objModel = $this->objService->getRecordById($id, $arrParams);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objModel);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return "edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $arrParams = $request->input();

        $objResponseBuilder = new ResponseBuilder();

        try {

            $objModel = $this->objService->getRecordById($id, $arrParams);

            foreach ($arrParams as $strField => $strValue)
            {
                $objModel->setAttribute($strField, $strValue);
            }

            $objModel->save();


            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objModel);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function replace($id)
    {
        //
        return "replace: " . $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return "destroy";
    }

    public function fullList(Request $request)
    {

        $arrPagination = $request->input('pagination');

        $arrOrdering = [];
        $arrOrdering['order_by'] = $request->input('order_by');
        $arrOrdering['order'] = $request->input('order');

        $arrResponse = [];

        try {
            $arrData = $this->objService->search([], [], $arrPagination, $arrOrdering);
        } catch (\Exception $e) {
            $arrResponse['status'] = 'error';
            $arrResponse['message'] = $e->getMessage();
            return $arrResponse;
        }

        $arrResponse['status'] = 'success';
        $arrResponse['data'] = $arrData;

        return $arrResponse;

    }

    public function getFullList(Request $request)
    {

        $arrOrdering = [];
        $arrOrdering['order_by'] = $request->input('order_by');
        $arrOrdering['order'] = $request->input('order');

        $objResponseBuilder = new ResponseBuilder();

        try {

            $arrData = $this->objService->getAll($arrOrdering);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($arrData);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function getPaginatedList(Request $request)
    {

        $arrPagination = $request->input('pagination');

        $arrOrdering = [];
        $arrOrdering['order_by'] = $request->input('order_by');
        $arrOrdering['order'] = $request->input('order');

        $objResponseBuilder = new ResponseBuilder();

        try {

            $arrData = $this->objService->search([], [], $arrPagination, $arrOrdering);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($arrData);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    public function search(Request $request)
    {

        try {

            $objUser = CurrentUser::getFromJWT();

            $arrFilters = $request->input('filters');
            $arrPagination = $request->input('pagination');

            $arrFiltersService = array();

            if (!empty($arrFilters) && is_array($arrFilters)) {
                foreach ($arrFilters as $strField => $strValue) {
                    if(str_contains($strValue,'%'))
                    {
                        $arrFiltersService[] = [$strField, 'like', '%' . $strValue . '%'];
                    }
                    else
                    {
                        $arrFiltersService[] = [$strField, '=',  $strValue ];
                    }
                }
            }

            $arrFiltersService[] = ["deleted","<>", "1"];
            $arrResponse = [];


            $arrData = $this->objService->search([], $arrFiltersService, $arrPagination);

        } catch (\Exception $e) {
            $arrResponse['status'] = 'error';
            $arrResponse['message'] = $e->getMessage();
            return $arrResponse;
        }

        $arrResponse['status'] = 'success';
        $arrResponse['data'] = $arrData;

        return $arrResponse;

    }


}
