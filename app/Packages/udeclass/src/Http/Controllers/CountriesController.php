<?php
/**
 * Ezesubu
 * 28/06/2017 5:51 PM
 */

namespace p7days2go\Http\Controllers;


use CountryState;
use p7days2go\Http\Response\ResponseBuilder;



class CountriesController extends GenericRestController
{



    function getCountries(ResponseBuilder $objResponseBuilder){

        try {

            $countries = CountryState::getCountries();

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($countries);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }

    function getStates($idCountry , ResponseBuilder $objResponseBuilder){

        try {

            $states = CountryState::getStates($idCountry);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($states);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();

    }


}