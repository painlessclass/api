<?php

namespace udeclass\Http\Controllers;

use Illuminate\Http\Request;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\ScheduleServices;

class ScheduleController extends GenericRestController
{
    function __construct()
    {
        $this->setService(new ScheduleServices());
    }

    public function getAllMatterSchedule(ResponseBuilder $objResponseBuilder)
    {

        try {
            $response = $this->objService->getAllMatterSchedule();

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }
}
