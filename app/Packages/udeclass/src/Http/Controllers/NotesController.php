<?php

namespace udeclass\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\NotesServices;

class NotesController extends GenericRestController
{
    function __construct()
    {
        $this->setService(new NotesServices());
    }

    public function createNotes(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $arrData = $request->input();

        try {
            $response = $this->objService->createNotes($arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function getAllNotes(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->getAllNotes($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function updateNotesById( ResponseBuilder $objResponseBuilder, Request $request)
    {
        $params = $request->all();
        try {

            $objMatter = $this->objService->updateNotesById($params);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objMatter);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }
}
