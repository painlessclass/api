<?php
/**
 * Ezesubu
 * 3/8/2017 6:29 PM
 */

namespace udeclass\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use udeclass\Http\Controllers\GenericRestController;
use udeclass\Http\Response\ResponseBuilder;
use udeclass\Services\CareerServices;


class CareerController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new CareerServices());
    }

    public function createCareer(ResponseBuilder $objResponseBuilder,Request $request)
    {

        $arrData = $request->input();

        try {

            $response = $this->objService->createCareer($arrData);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setStatusCreated($response);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }



        return $objResponseBuilder->buildResponse();

    }

    public function getAllCareer(ResponseBuilder $objResponseBuilder)
    {

        try {

            $objCareer = $this->objService->getAllCareers();

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($objCareer);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

    public function deleteCareerById(ResponseBuilder $objResponseBuilder, Request $request)
    {
        $data = ((array)json_decode($request->input('data'))) ;
        try {

            $reponse = $this->objService->deleteCareerById($data);

            $objResponseBuilder->setStatusSuccess();
            $objResponseBuilder->setData($reponse);

        } catch (BaseException $e) {
            $objResponseBuilder->setFromBaseException($e);
        } catch (\Exception $e) {
            $objResponseBuilder->setFromFatalException($e);
        }

        return $objResponseBuilder->buildResponse();
    }

}