<?php
/**
 * Jaime
 */

namespace udeclass\Http\Controllers;


use Illuminate\Http\Request;
use p7days2go\Http\Response\ResponseBuilder;
use p7days2go\Services\HelperService;

class HelperController extends GenericRestController
{

    function __construct()
    {
        $this->setService(new HelperService());
    }

    public function getDateTime()
	{
		$strDateTime = $this->objService->getServerDatetime();
		
		$objResponseBuilder = new ResponseBuilder();
        $objResponseBuilder->setStatusSuccess();
        $objResponseBuilder->setData($strDateTime);

        return $objResponseBuilder->buildResponse();
	}


}