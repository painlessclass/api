<?php
/**
 * TiTo
 * 3/10/2017 10:35 PM
 */

namespace udeclas\Exceptions;


class BaseException extends \Exception
{

    protected $key = null;
    protected $description = null;

    public function __construct()
    {

        $arrArgs = func_get_args();

        switch (count($arrArgs)) {
            case 1:
                $this->description = $arrArgs[0];
                break;
            case 2:
                $this->key = $arrArgs[0];
                $this->description = $arrArgs[1];
                break;
        }

    }

    public function getKey(){
        return $this->key;
    }

    public function getDescription(){
        return $this->description;
    }

}