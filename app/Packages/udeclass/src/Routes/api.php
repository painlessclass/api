<?php

use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use udeclass\Http\Controllers\CareerController;
use udeclass\Http\Controllers\MatterController;
use udeclass\Http\Controllers\ScheduleController;
use udeclass\Http\Controllers\HomeworkController;
use udeclass\Http\Controllers\TestController;
use udeclass\Http\Controllers\NotesController;
use udeclass\Http\Controllers\ForumController;

Route::post('sign_in', LoginController::class . '@login');
Route::post('users', RegisterController::class . '@create');
//reset password
Route::post('password/email', ForgotPasswordController::class . '@sendResetLinkEmail');
Route::post('password/reset', ResetPasswordController::class . '@reset');

Route::post('career', CareerController::class . '@createCareer');
Route::get('career', CareerController::class . '@getAllCareer');
Route::delete('career', CareerController::class . '@deleteCareerById');


Route::get('matter', MatterController::class . '@getAllMatterByClientId');
Route::get('matter/public', MatterController::class . '@getAllMatterPublic');
Route::post('matter', MatterController::class . '@createMatter');
Route::post('matter/subscription', MatterController::class . '@subscriptionUserToMatter');

Route::get('schedule', ScheduleController::class . '@getAllMatterSchedule');

Route::get('forum', ForumController::class . '@getFromMatter');
Route::post('forum', ForumController::class . '@store');

Route::get('homework', HomeworkController::class . '@getAllHomeworkByIdMatter');
Route::post('homework', HomeworkController::class . '@createHomework');
Route::put('homework', HomeworkController::class . '@updateHomework');

Route::post('test', TestController::class . '@createTest');
Route::get('test', TestController::class . '@getAllTest');
Route::put('test', TestController::class . '@updateTestById');

Route::post('notes', NotesController::class . '@createNotes');
Route::get('notes', NotesController::class . '@getAllNotes');
Route::put('notes', NotesController::class . '@updateNotesById');



