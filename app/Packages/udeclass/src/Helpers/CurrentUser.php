<?php
/**
 * TiTo
 * 3/7/2017 11:54 PM
 */

namespace udeclass\Helpers;


use Tymon\JWTAuth\Facades\JWTAuth;

class CurrentUser
{

    public static function getFromJWT(){

        return JWTAuth::parseToken()->authenticate();

    }


}