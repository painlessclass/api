<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JWTAuth;
use udeclass\Http\Response\ResponseBuilder;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $objBuilder = new ResponseBuilder();
        try {

            if ($request->rememberme) {
                config(['jwt.ttl' => (60*24*60)]);
            }
            if (!$token = JWTAuth::attempt($credentials)) {
                $objBuilder->setStatusError('INVALID_CREDENTIALS', 'Nombre de usuario o contraseña no valido');
                return $objBuilder->buildResponse();
            }
        } catch (JWTException $e) {
            $objBuilder->setStatusError('COULD_NOT_CREATE_TOKEN', 'Error interno de servidor al crear el token');
            return $objBuilder->buildResponse();
        }

        return response()->json(compact('token'));

    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
