<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Matter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('matter')
            ->insert([
                ['name' => 'Matematicas', 'create_by' => 0, 'id_career' => 1],
                ['name' => 'Filosofia', 'create_by' => 0, 'id_career' => 1],

                ['name' => 'Religion', 'create_by' => 0, 'id_career' => 2],
                ['name' => 'Etica', 'create_by' => 0, 'id_career' => 2],

                ['name' => 'Probabilidad', 'create_by' => 0, 'id_career' => 3],
                ['name' => 'Gestion', 'create_by' => 0, 'id_career' => 3],
            ]);
    }
}
