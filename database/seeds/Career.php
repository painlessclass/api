<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Career extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('career')
            ->insert(
                [
                    ['name' => 'ING SISTEMAS' ],
                    ['name' => 'ADMON' ],
                    ['name' => 'CONTADURIA P.' ]
                ]
            );
    }
}
